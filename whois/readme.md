Access to Public Interest Registry WHOIS information is provided to assist persons in determining the contents of a domain name registration record in the Public Interest Registry registry database.

In other words, WHOIS lets you get owner details of a website if they did not use private registration.

Depending on your operating system you may be able to run it via terminal like this:

```
whois example.com
```

Or you can your favorite search engine and search for a WHOIS site.
